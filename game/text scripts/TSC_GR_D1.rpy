label GR_0:

#“Hello Aaron, I hope we can learn a lot from each other.” (Best)

    "Aaron looks up at me and gives a faint smile"
    "He seems to appreciate the kind gesture"

    A "That...would be nice…"
    A "I hope I can a lot from you as well..."

    "Monika smiles at us both"
    "She seems to appreciate that I'm doing my best to make this guy feel welcome"
    "Must take a lot of the stress off of her sholders if I lend out a hand..."

    M "It’s wonderful to see how quickly you’ve all acclimatized."
    M "Now, before everyone takes up their usual activities-Aaron, would you tell us a bit about yourself?"

    "Aaron’s attention swims back to the far wall and windows again, like we’re not there."
    "I think he’s thinking of something to say."
    "Like if thinks we would be too judgemental..."
    "Introductions can be tough, I’ll say ..."

    A "Well..."
    A "I can’t really say I’m the interesting sort…"
    A "I came to Japan to learn more about Manufacturing and Business..."
    A "I always thought it would have been cool to be one of those guys who built things..."
    A "...Like refrigerators or car parts..."
    A "Stuff that requires understanding on machinery and electric wiring..."

    S "Woah!"
    S "That sounds super complicated."

    A "Oh, yes."
    A "I expect the worst to be thrown my way..."
    A "But with enough studying and maybe some intern work, I should be able to get the qualifications to at least work on the industrial line…"

    Y "Industry?"
    Y "Sounds like to me you’re more of a worker than a thinker…"

    "Yuri realises that she just said that out loud and tries to correct herself, but it's too late for her to take that back..."

    A "Mmm...can’t argue in all honesty…"
    A "I like work where I know what I’m doing and where improvising happens at a minimum."
    A "However I do like to learn about new things, weather it be something that happened in world history..."
    A "Or even something as simple as getting into a new series..."

    M "Sounds like you already got everything figured out, with enough work and studying you’ll be there for sure!"

    "Everyone smiles at Monika’s enthusiasm, including myself."

    M "Well people, you’re all free to do whatever you’d like today, as long as it’s literature related."
    M "But before you start, please keep in mind I’ll be giving out an assignment that will be mandatory for everyone here."

    "Everyone breaks up and starts doing their own thing..."
    "Per usual- Monika sits at the Teacher’s desk reading a book..."
    "Yuri and Sayori sit together near the back of the classroom talking about a new book Yuri bought last week..."
    "Natsuki is... walking up to Aaron?"
    "After being so harsh with him literally a few minutes ago?"
    "Maybe she’s apologizing for how harsh she was with him eairlier and realized so after Aaron explained himself in a better light..."

    "Anywho, I should probably see what everyone’s up to since I don’t really have anything to read..."
    "Who should I check up on first?"


#Monika

#Yuri and Sayori

#Aaron and Natsuki

#-Monika-

    "I approach Monika, who seems to be deeply invested in her book..."

    Mc "Hey Monika, whatcha reading?"

    "She flinches a bit, I guess she didn’t hear me walk up to her..."

    M "Ah- oh…"
    M "Sorry about that,[player]..."
    M "I'm just reading this corny romance novel that I found in a 'bargain bin' the other day…"
    M "It’s actually really bad..."
    M "So bad that I just can't stop reading it, cliches are everywhere..."

    Mc "I guess it's more of a comedy book than a romance novel..."
    Mc "...A 'How not to...' I would assume."

    M "Yeah…"
    M "This is WAY too generic for my tastes…"
    M "Anywho... I wanted to ask..."
    M "What do you think about him?"

    Mc "Who?"
    Mc "Aaron?"
    Mc "He seems to be pretty cool..."
    Mc "You don't really see enough people nowadays who have the drive to do something like Industrial Labor..."
    Mc "He seems to know what he wants out of life at the very least…"

    M "Yea, hopefully he’ll be as invested in the club as much as he is when it comes to his Career…"
    M "I just hope he has enough time to both balance his studies and his time here..."

    MC "He should have plenty of time, he wouldn’t join our club if that wasn’t the case, right?"

    M "I suppose you might be right…"
    M "But I must ask you…"
    M "I'm not sure if everyone else noticed it or if it's just me...but..."
    M "...Do you think he’s a bit too nervous?"

    Mc "Well, he is new to the country and here he is presenting himself to a bunch of people he’s never met before."
    Mc "Remember my first day of the club and how I nearly had everyone get upset that I might not actually join?"

    M "Of course I do, just imagine how heartbroken Sayori would’ve been if you decided to leave..."

    "I think about it for a moment…"
    "It’s almost hard to believe it’s been an entire year since I first joined the club in my Senior year…"
    "How time flies..."

    Mc "I wouldn’t hear the end of it…"
    Mc "But I sure am glad I chose to stay."
    MC "Being a part of this club did bring me some of the happiest days of my life thus far..."
    Mc "Maybe Aaron will experience the same feelings that I did back then and even up to today?"

    "Upon hearing that, Monika responds by giving me a big, warm smile"

    M "You don’t know how much it means to me to hear that, [player]..."
    M "Especially coming from you."
    M "If I can make even one person’s day better just by leading a literature club..."
    M "Then I can certainly rest happy at night knowing I made someone else's day better."
    M "As for Aaron, I hope he will be comfortable here."
    M "It must be hard for him to adjust to such a different place with such a different culture..."
    M "But at the very least, we should open up to him with open arms!"

    Mc "I might not be able to speak for everyone..."
    Mc "But I can at least guarantee I’ll do my best to make him feel welcome here!"

    M "Thanks, [MC]."
    M "I know I can count on you!"



-Yuri and Sayori-

    "I make my way to Yuri and Sayori, who are talking about some book on the desk."

    MC "Hey, you two.  What’s going on here?"

    s "Oh! Hey [player]!"
    s "Yuri here is telling me all about this new book she got!"

    "The book has the silhouette of a bird of some sort engraved into the cover, with the French Flag superimposed behind the bird outline."

    y "‘Day of the Crows’."

    MC "Cool. What’s it about?"

    y "Why, I’m glad you asked [player]."
    y "Essentially..."
    y "A man in 17th century France finds himself accused of breaching the law and so he becomes a part of this gang..."

    mc "Sounds interesting."

    s "I think so!"

    y "So far it seems to be a parody-type novel."
    y "Think Don Quixote meets a resistance plot."

    "Sayori and I both give Yuri confused looks."
    "However Aaron’s head momentarily pops out of the manga he's reading with Natsuki, then returning back to it before she can notice he lost focus..."

    y "..."
    y "You don’t know who that is, do you."

    s "Nope!"
    s "Sounds like it’s fun to read though, Yuri!"

    "Yuri shrugs, setting the book in her lap."

    y "Well, it’s mostly a comedy based narrative..."
    y "Not really something I would normally read, but it’s a small break from all my horror novels..."
    y "The setting is good enough to at least intice me..."

    MC "Well, you two have fun, I'm gonna see what everyone else is up to..."

    s "See ya later, [player]."


-Aaron and Natsuki-

    "I walk over to Aaron and Natsuki..."
    "Aaron sitting at a desk, writing inside a Dark-red hardcover book..."
    "Natsuki standing beside him…"
    "From what it seems like, Natsuki hasn't realized I showed up..."
    "As for Aaron, he's so focused on his writing, he hasn't noticed either of us..."
    "Why is Natsuki just standing here then?"
    "It's not long after I approach that Aaron looks up."

    A "...O-oh, hey, [player], hello Natsuki..."

    "Aaron is the first to notice me, Natsuki turns around after being alerted of my presence"

    n "[player]?"
    n "What are you doing here?"

    mc "Nothing really, just thought I’d check up on you two…"

    n "Is this about what happened earlier?"
    n "Because... well-..."

    "Natsuki looks down at the floor, embarrassed to have to do this in front of both Aaron and myself..."

    n "I'm sorry for the way I acted earlier..."
    n "It's just that I take this club very seriously and I want everyone else to do the same..."
    n "I guess I didn't realise that trying to keep the flow of the club going that I came off as a bitch..."

    "I didn't come over here to make sure she made up for her actions earlier, but I'm glad she did."
    "I know Natsuki likes thing to stay the same in regards to the club..."
    "It must be kinda hard to keep your cool when someone new disrupts the way things used to be..."

    n "I suppose Monika was right that I was a bit harsh…"
    n "No hard feelings, OK?"

    a "Oh no no no, Natsuki… everything's fine."
    a "If anything, what you said before was true..." 
    a "It's true that I am new here..."
    a "But that's no excuse for me to take any slack..."
    a "I have to do as much as everyone else in this club."
    a "Besides, I don’t like taking things I didn't earn..."
    a "...slack being one of them..."

    n "Really?"
    n "Well...that’s good to hear!"
    n "At least we both know that I wasn't being mean..."
    n "Did you happen to bring anything to read?"

    a "I’m afraid not…"
    a "I did bring a book with me..."
    a "But I ended up finishing it one the plane ride to Japan..."
    a "The only other stuff I came here with were some clothes, supplies, a journal and an art pad."
    a "That's pretty much everything I own..."
    a "...At least for the time being..."

    n "What?"
    n "What do you even plan on doing here then?"
    
    a "I guess just write-..."
    
    n "ERRR- Wrong!"
    n "This won’t do..."
    n "You can’t just sit here and write all day!"
    n "If you don't have anything to read, then I suppose it’s up to me to give you something to read!"
    
    a "That's nice and all but..."
    
    n "...'But' what?"
    
    a "I..."
    
    "Aaron looks at the ground once again, looking ashamed in what he's about to say..."
    
    a "I don't know how to read Japanese..."
    
    n "Is that it?"
    n "Well then, I can help you with that!"

    "Both Aaron and I are a bit surprised with Natsuki’s declaration..."
    "She's really going to teach Aaron how to read, just like that?"

    a "Wait..."
    a "Are you offering to teach me?"
    a "I mean..."
    a "I am really grateful for the offer, but you really don’t need to-"

    n "Nonsense!"
    n "Someone has to do it, and if anyone is going to teach you, they might as well teach you with some high quality literature!"
    
    "Natsuki then presents Aaron a book, one I'm all to familar with: 'Parfiet Girls: Volume 1'" 
    
    n "It’s going to be me who'll be the one to teach you!" 
    n "No question or backing out of this, understand?"

    "Aaron looks over to me, he looks both confused yet humored, then he looks back at Natsuki, who has Determination written all over her face."

    a "Well…"
    a "If you really want to teach me, I don’t see why I should decline..."
    a "Besides, this manga looks really interesting..."

    n "Great!"
    n "Japanese manga Session begins now!"
    n "Bug off [player], we got a lot of work to do and we don’t need any distractions!"

    mc "Hehe, Alright you two, good luck and have fun..."


#-----------------------------------------------Afterwards-------------------------------------------------------

    	"After checking on everyone, I return to my seat and do some reading required for my Literature class until Monika stands up and stops us from everything we’re doing..."

    m "Ok everyone, we’re about done for today, If we can all come to the center of the room, I will explain everyone’s assignment for tonight…"

    n "Aw geez… Just when it was getting good..."
    n "You're learning fast by the way..."

    a "Thank you...there’s always tomorrow, right?"

    n "Not so fast, you’re not done yet!"
    n "You’re going to take this issue back to your dorm and finish it before we come back here tomorrow."
    n "You’re not half as bad as I thought..."
    n "You should be able to get through this issue by tommorow in time for your 'pop quiz'!"
    
    a "'Pop quiz' ay?"
    a "Well... ok..."
    a "I'll try to finish this issue using what you taught me so far..."

    "Natsuki hands over her first issue of Parfait Girls to Aaron, which he holds gently, looking at the cover."
    "I still remember reading that issue with her roughly one year ago..."

    a "Are you sure you’re ok with giving me this?"

    n "Yea, just take good care of it"
    n "After your quiz, we can get started with the second book." 
    n " But if I don’t see this book in better condition than how I gave it to you, I’ll-"

    m "C’mon you two, we’re waiting..."

    n "... Just bring it back in good condition, ok?" 

    a "Understood."
    a "Thank you again, Natsuki!"

    "Everyone including myself now stands in front of Monika, awaiting what our assignment will be..."
    "...Call it a hunch...but I have an idea on what it’ll be..."

    m "Ok, everyone!"
    m "Most of you here will remember that during our first week of club last year we all did poetry as a way for all of us to get to know each other better."
    m "Since we have Aaron here this year, I figured now’s the best time to take another whack at it..."
    m "So tonight and every night until further notice, we all have to write a poem to share with everyone!" 

    s "Woohoo!"
    s "Poems!"
    s "It’s been forever since we last did them!"

    a "Poems huh?"
    a "That's going to be a bit of a challenge..." 

    y "You don’t need to worry, Aaron..."
    y "Everyone has to start from somewhere, and not everyone can make a masterpiece at their first attempt."

    n "At least make sure yours are decent1"
    n "[player] made some really trashy ones last year..."
    n "He even read one of his in front of a whole audience while they all fell asleep."

    "Natsuki looks me straight in the eyes as she smirks, I know for a fact my earlier poems weren’t THAT bad..."

    m "Just try your best Aaron..."
    m "Here’s some advice..."
    m "When it comes to poetry or writing in general, it’s always best to write things you’re passionate about."
    m "A hobby or something you like for example."
    m "Maybe even about {i}someone you like{i}..."
    m "If you don’t write about something you particularly like, you can’t guarantee you’ll put as much effort into it..."
    m "..and your work may suffer because of it..."
    m "So in the end, make sure you have fun when you’re writing."
    m "That’s Monika’s writing tip of the day, I’ll see you all tomor-"

    s "Wait a minute everyone!"

    "Everyone stops and stares Sayori as she interrupts..."

    s "Hehe~ sorry..."
    s "Before anyone leaves, I wanted to make a suggestion..."
    
    m "Oh?"
    m "Please share it with use Sayori..."

    s "Well..."
    s "Since today is the 1 year anniversary of our club..."
    s "I thought we could go out tonight?"
    s "You know... as a way to celebrate!"
    
    m "Oh yea... It has been a year since we all gathered up... with the exception of Aaron, of course..."
    m "That sounds like a wonderful idea Sayori!"
    m "What does everyone else think?"
    m "Should we all go out tonight to celebrate this special occasion?"

    s "Ohhhhh Yess! You’re the best, Monika!"

    mc "Well, I am a little bit hungry..."

    "I’m actually starving, but I didn’t want to say it out loud..."

    y "That sounds like a great idea..."

    n "Count me in, let’s do it!"

    m "Well then, it's settled…You coming with us, Aaron?"

    a "I would go... but I don't have any Yen..."

    m "You don't need to worry about that."
    m "Since I'm the president, I'm covering tonight..."

    a "..."
    a "...You guys actually want me with you?"

    m "Of course!"
    m "You're part of the club too..." 
    m "Even if you’re new member, we would still welcome you."

    a "...Right, well, I don't see why not..."

    m "Great!" 
    m "I'm going to take you guys to this great place down from where the campus is at."
    m "I hear it's popular with the other students!"
    
    - Screen transitions to black -

    "We all gather our belongings, we take everything we don't need to our rooms and meet up at the Campus entrance to walk into town..."

    - Screen transitions to a Restaurant Booth -

    "As soon as we get off the bus Monika leads us down to a fancy-looking Japanese restaurant."
    "We all enter inside and get ourselves seated"
    
    y "Wow... this place looks amazing"
    
    a "Much fancier than our 'Americanized asian restaurants.."

    mc "Well, what is everybody getting?"

    s "Ohhhh man... there's so much here..."
    s "I just don't know what to pick..."

    mc "Just don't order so much Sayori, just because Monika is treating us doesn’t mean you can just order one of everything..."

    s "Give me some credit [player], I wouldn't do something so selfish..."

    mc "Alright.."
    mc "As for me, I think I will have the..."

#Yakitori  (Kebabed chicken)

#Tempura  (Fried vegetable)

#Miso Soup  (Soup)

#Mochi  (Rice cake)


     m "Ooh, that actually sounds good… who still hasn't figured out what they wanted yet?"

    mc "What do you want to get Aaron?"

    a "I don’t know what to get..."
    a "I never had Japanese food before..."

    n "Are you serious???"
    n "Okay, we have to get you something good!" 
    n "I'm gonna order you the Yakitori and you're not going anywhere until you finish it all, got it?"
    
    "Aaron laughs to himself, half nervously and half in amusement..." 

    a "...Uh, certainly..."
    a "Sounds like a good plan to me."

    "Everyone else finished their orders." 
    "We all fold up our menus and place it at the center of the table."
    "We then tell the waitress what we wanted and wait for our food to arrive."

    m "So I forgot to ask during the club, how was everyone's first day?"

    "soft piano music began to play in the background, which really helped everyone sort of loosen up while sitting in our kinda cramped booth"

    s "Ooh! Me first!"

    mc "Sayori, take it away."

    s "Well, today was fun! All my classes are going to be great!"

    m "That’s great, Sayori!"

    y "Well what about you, Monika?"

    m "Well, things are easy so far..."
    m "No real homework, just basic handouts and syllabi."

    "Natsuki pouted, resting her heads on her head and frowning slightly."

    n "Yeah, nice to hear."

    mc "You don’t sound very enthusiastic…"

    s "What?"
    s "Your day not going as well?"

    n "It’s not that it was bad, I just have a one and a half page essay DUE TOMORROW!"

    y "Doesn’t sound so bad."

    m "You sound confident, Yuri."
    m "Why is that?"

    "Yuri grins slyly."

    y "Because I have two."

    "Laughter erupted among the small click, a loud, united and boisterous laughter consuming the small booth..."
    "...Even Aaron chuckles along with us."

    n "At least my culinary class is going to be decent..."
    n "I get to do what I do best there!"

    s "Wait, they have a class for FOOD?"

    mc "Of course that’s what you really care about..."

    n "Speaking of which, WHERE’S THE FOOD?"

    y "Natsuki, please calm down..."

    m "Monika’s 'second' Tip of the Day!:‘Patience is a virtue.’"

    s "But that’s a boring virtue!"

    "A second burst of laughter comes bursting out."
    "But it’s quickly quelled, as plates of food begin to come to the table." 

    s "Yay~!"

    n "Finally!"

    mc "Bon appetit!"

    "Kebabed chicken, rice cakes, soup, sushi and vegetable plates are passed and slid quickly around the table."
    "The smell was glorious, wafting up from our respective plates into a fanfare of deliciousness."

    a "...Uh, Sayori?"
    a "What kind of food is that?"

    "Sayori’s head snapped up, no longer stuffing her face with mochi."

    s "Oh, right!"
    s "You’ve never really HAD Japanese food, have you?"

    "Aaron nods slowly."

    s "Well, this right here is a Mochi!"
    s "...It’s a rice cake, steamed overnight and pounded with hammers until molded into squares!"
    s "And apparently here, they serve it with ice cream!"

    a "Is it meant to be a dessert, or a meal?"

    s "Yuri, do you know?"

    y "It’s traditionally a speciality for New Years celebration..."

    mc "But Sayori eats it however and whenever she wants to."

    s "You know it!"

    n "I’m having a small bowl of Miso soup."
    n "I think it has mushrooms, potatoes and shrimp in this bowl!"

    a "Ohhh..."
    a "...that sounds really good"

    n "You better believe it is!"

    a "Yuri, is your chicken the same as mine?"

    y "Yes, yakitori is just kebabed chicken with spices on top."

    m "And I’m having fried vegetables." 
    m "It’s called tempura."

    "We eat in silence for an indeterminable amount of time, enjoying each other’s company."
    "I clasp my hands together, ignoring my food for the moment."

    mc "Where were we?"

    s "I believe we were at you for talking about our first days!"

    "I shrug."

    mc "Ehhn. It was fine. Not really much of interest for me."

    m "So that just leaves…"

    n "Aaron!"

    a "Oh-...Umm..."

    "He pauses for a little bit, presumably to gather his nerves."

    a "Well..."
    a "Almost all my classes are either Business related or Technology related..."
    a "So it’s primarily note based learning..."
    a "...At least so far, anyways."

    n "That’s cool!"

    m "Certainly unique, Aaron!"

    mc "Yeah, man!"

    "Everyone is sitting down, everyone finished with their food." 
    "I think Sayori’s been done since we started talking..."

    all "..." 

    m "Well, I say that’s a dinner well spent!"
    m "Speaking of spend…"

    "Monika pulls out her coin purse..."

    m "I’m gonna take care of the bill, guys."


    #Let Monika take the bill.

    #Refuse to let Monika pay for it all.

    # -Let Monika take the bill.-

    "Monika calls over the waiter, takes out the money from her purse, and hands it over to the person."

    # -Refuse to Let Monika pay for it all.-

    mc "Monika, let me help pay."

    "I quickly fish through my pockets for some spare Yen."

    m "No [player], it's fine..."
    m "let me get the tab taken care of..."

    s "Come on, let us chip in a little, Monika!"

    n "You took us here for an awesome time!"

    y "The least we could do is offer out our coins too."

    m "Aww, thanks you guys!"

    a "If I had Yen, I would pitch in too..."

    mc "It’s okay, man."

    n "Yeah, don’t worry about it, Aaron!"

    "Aaron looks down embarrassed but not for too long, he looks back up and accepts it for what what it is..."
    
    a "Very well.."

    "Monika calls over the waiter, takes out the money from her purse, and holds out her hands."
    "We all put our coins in with hers and hand it over."


            #-------------------------------------------------------------------------------------

    #-show Uni entrance at Night

    "We make our way back to the campus, our own little Literature herd."

    m "Be sure to get that poem done for tomorrow!"
    m "See you guys tomorrow!"

    Everyone "Bye!"

    #- Screen transitions to a Dormitory hallway -

    "We all split up, making our separate ways to our dorms."
    "I make it to mine, pulling out the keys from my pocket and letting myself in..."
    
    #- Screen transitions to a Dorm Room -
    
    "I set my keys up on the keyhook and sit down on the edge of my bed."
    
    mc "Poems again huh..."
    mc "..."
    mc "Heh-... alright..."
    mc "Let’s get this poem done with!"
    
    #- Screen transitions to poem game -

