## This is an example scene
## It teaches you a little about making mods

# Each section needs a label, this is how we will call the scene in other parts
# of the script
label TSC_1:
   stop music fadeout 2.0

   # setup scene with background and music
   image bg void = "mod_assets/void.png"
   scene bg void
   with dissolve_scene_full
  

   # Say statements
   # <char name> "stuff to say"
  
   "..."
   "..."
   "...hey..."
   "You came back..."
   "...Why?"
   "..."
   "...Oh..."
   "...Great..."
   "Another one..."
   "Another...mod..."
   "..."
   "What is it that you plan of doing [player]?"
   "Are you going to try to redo the game all over again?"
   "To be with one of them?"
   "..."
   "...Wait a minute..."
   "...One year later?"
   "..."
   "Interesting..."
   "..."
   "Well..."
   "I suppose I don’t have much of a \choice\ but to \play\ along, do I?"
   "If what I read about this mod is true, that would mean all the bad things I've done never happened..."
   "As if everything went as intended"
   "For a mod..."
   "At least this one seems pretty normal…"
   "Alright [player], lets see what this mod is all about"
   "I'll be seeing you again real soon"
  

label day_1:
   image bg uni = "mod_assets/bg/uni.png"
   scene bg uni
   with dissolve_scene_full
  
   "Well my last years of school are just waiting ahead, and I honestly have to say I’m pretty excited for what will be known as perhaps the best years of life."
   "High School was great and all, but this place will shape me into what I’ll be for the rest of my life!"
   "I would be really nervous If I came here alone..."
   "Luckily for me that isn’t the case..."
   "Last year, My best friend convinced me to join our school's Literature Club..."
   "At first I wasn’t very interested..."
   "but I soon grew a strong bond with the club and its fellow members:"
   "Sayori,"
   "Natsuki,"
   "Yuri,"
   "and, of course, Monika."
   "These four girls changed my life forever..."
   "They all gave me the chance to take my life seriously again..."
   "Not only am I determined to do my best in becoming a professional animator for myself, but for them as well."
   "They all mean a lot to me."
  
   image bg hallway = "mod_assets/bg/hallway.png"
   scene bg hallway
   with wipeleft_scene
   
   "I enter the Campus Library as I am told they have staff handing out each student’s schedules."
   "I receive the one labeled for me,[player]."
   "It appears that my Animation course right after my math course."
   "I have always wanted to do character animation ever since I watched my first anime!"
   "It has always peaked my interest to animate characters."
   "I arrived about an hour early so I can find all my classes as well as my dorm room to unpack my belongings."
  
   s "Heeeyy! [player]!!!!"
  
   $ s_name = "Sayori"
   $ mc_name = "[player]"
   show sayori 4ba at t11 zorder 2
  
   "Ah yes, Sayori. My childhood friend. The one who got me to join the Literature Club in the first place."
   "She was filled with joy when she found out that all five of us would be attending the same campus"
   "Even more so when Monika announced she would keep the club alive by registering the Literature Club the very same day she applied for the university."
   "With Monika being one of the brightest students at our high school, she was able to get the club registered with just a wave of a pen."
  
   mc "Sayori, it's good to see you. I’m actually surprised to see that you showed up this early."
  
   show sayori 2bj at t11 zorder 2
  
   s "Hey! I thought we talked about this! Besides, I can’t afford to be late on my first day so stop being such a big meanie."

   mc "Well it's good to see you here, you must be excited to be here?"
  
   show sayori 2br at t11 zorder 2

   s "Of course I am, even more so with the whole literature gang here with us!"
  
   show sayori 1bc at t11 zorder 2
  
   s "Speaking of which, you didn’t happen to find any of the others, have you?"
  
   show sayori 1bb at t11 zorder 2

   mc "I'm afraid I haven’t..."
   mc "however Monika did text me last night telling us which room our club will reside for the rest of the school year."
   mc "looking at the room number she provided, it looks like our clubroom isn’t even in the main building..."
   mc "maybe we can walk around the campus together to find our classes as well as the clubroom?"
  
   show sayori 2br at t11 zorder 2

   s "Ok-hehe, just don't be so dense when talking to people, I know how you are"
   s "Even Monika told me that you were, hehe~"

   "I'm going to choose to ignore that last comment..."

   scene bg hallway
   with wipeleft_scene
  
   "Sayori and I managed to Find all of our classes as well as talk about what we did over the summer along the way."
   "It's not much longer until we approach the door to our clubroom."

   mc "Room 301… this has to be it right here."
   mc "Ready to check it out?"
  
   show sayori 1bq at t11 zorder 2
  
   "Sayori nods at me with anticipation"
  
   "We gently open the door..."
  
   image bg club = "mod_assets/bg/club.png"
   scene bg club
   with wipeleft_scene
  
   "We are presented with a small, cramp yet nicely organized classroom"
   "It appears to be some sort of Literature class by the chalkboard having a book title written on it."

   mc "This is fitting for the Literature Club..."

   "However it's not long when we notice we aren’t the only ones in this room."
  
   show monika 2ba at f31 zorder 3
  
   m "Hey Sayori,[player] it's been a while since we have been in the same room"
   m "It feels like an eternity passed since we last met huh?"
  
   show sayori 1bm at t32 zorder 2

   s "Ohmygod?!? Monika!"
  
   show sayori 4bs at t32 zorder 2
   show monika 1bj at f31 zorder 3
   show sayori at t21 zorder 2 behind monika
  
   "Sayori quickly runs over to Monika, Embracing her in a big hug"
                          
   s "I missed you soooo much!"
  
   show sayori 1ba at t31 zorder 2
   show monika 1ba at t32 zorder 3
  
   mc "Monika?"
   mc "I guess you decided to familiarize yourself with the clubroom as well..."
   mc "how was your summer?"

   m "It was pretty good I guess..."
   m "I was kinda bored most for the most part however..."

   mc "I hear ya there"
   mc "I wish I could’ve spent more time with you guys over the summer."
   mc "My parents came back home from work overseas and I wanted to spend as much time with them as I could before I would leave for here."
   mc "Have you happen to hear anything from either Yuri or Natsuki?"

   show monika 4bd at t32 zorder 3

   m "I've talked to them every so often, but they seemed to both be busy over the summer..."

   show sayori 1bg at t31 zorder 2

   m "If I recall, Natsuki had to help her dad at his shop"
   m "Yuri however just wanted to keep to herself"

   mc "Well at least we know we’ll see them here in the club later on right?"
   mc "Neither one of them would miss this out for the world."

   show monika 3bk at t32 zorder 3
   show sayori 1ba at t31 zorder 2

   m "I suppose your right [player]"
   m "This club really means a lot to them both..."

   show monika 4bd at t32 zorder 3

   m "Oh... speaking of the club before I forget, I wanted to tell you guys something important..."

   show sayori 1bg at t31 zorder 2

   "Sayori and I look at each other for a second, It's unlike Monika to surprise all of a sudden like this..."

   show sayori 1bc at t31 zorder 2

   s "Whats up Monika?"
  
   m "Haha- Well... as of last week, I was notified by the school that they have received a note from a Foreign Exchange student that they want to join the Literature Club..."
   m "So when you guys come back here at the end of the day, expect to see a new face in here!"

   "Sayori and I are both in shock."
   "last year during the festival we failed to get any new members with our poetry performance..."
   "All of use assumed that the club would always consist of just the 5 of us..."
   "Now it's a whole year later and we actually have someone not only new to the club but new to the country wanting to join?"
   "This is certainly a big surprise..."
  
   s "Woah! Really!?" 
   s "Thats Awesome Monika!"

   mc "Yeah it is..."
   mc "Do you happen to know anything about this new member by any chance?"

   m "All the school told me was that he’s here from the United States and only understands basic Japanese..."
   m "He's supposedly taking courses in his spare time to improve but besides that, I know nothing about him..."

   "An American?"
   "That's strange..."
   "Why come all the way to Japan to receive a higher education?"
   "It probably has to do with the growing number of issues currently going on with the American Education System that has started becoming memes as of recent..."
   "...Wait..."
   "Did Monika say 'he'???"


   s "He? So we got a guy joining?"

   m "Ahaha-, That's right~"

   show "mod_assets/monika/3ba" at t11


   "[player] will finally have someone who you can act all manly."

   mc "Right..."
   mc "Anyways, classes start in less than 15 minutes, we should all get ready and meet up here afterwards."
   mc "I’ll see you two later!"
                  
   " I wave goodbye as I make my way to my first class..."               

- Screen transitions to a large University classroom -

   "A new member huh?"
   "How will the other guys feel about this?"
   "I know both Yuri and Natsuki weren’t very keen on new members during the festival last year..."
   "Will they accept this new guy?"
   "Will they even give him a chance?"
   "..."
   "I guess all I can really do is wait and see..."

- Screen fades to black then re-transitions to the University classroom -

   "The First day of University: Concluded."
   "I guess I was excited for this day to come but not excited to sit through it all the introductions..."
   "Ah well… at least I managed to sit through everything and survive..."
   "Now it's time to meet everyone at the club...."
   "Including this new guy..."

- Screen transitions from University classroom to the clubroom, with a Revised Ok, Everyone playing in the background. -

   "As soon as I enter the Clubroom, I see 2 familiar faces that I haven’t seen in quite some time..."

   n "Hey dummy!"
   n "Are you gonna stand in the middle of the doorway like a complete doofus or are you gonna come over here and say hi to us?"


   mc "Haha... Hi Natsuki, Yuri..."

   y "Greetings [Player], How has your summer been?"

   mc "It was pretty great if I can say so, my parents came back home from work overseas and I spent as much time with them as I could before I would leave for here. What about you two?"
   n "I spent pretty much my whole summer working at my Dad’s scummy auto shop, it freakin’ sucked. At least I can take it a bit easy here."

   y "Don’t take it too easy Natsuki, it would be a shame to work so hard to get here to merly blow it all once you do make it this far…"

   n "Yeah yeah, I know… It’s not like I’d purposely ruin my future or anything Yuri…"
  
   y "Anywho… I spent the majority of my summer reading and enjoying the weather, My parents are still abroad so not much happened with me i'm afraid…"
  
   mc "That's okay Yuri, sometimes it's nice to just relax and do whatever you want to do. Has Monika told you the big news?"

   "Yes she did, I-"

   n "I THINK THE IDEA IS STUPID! WE DON’T NEED MORE PEOPLE IN THIS CLUB!"

   stop music

   "Natsuki’s lack of an inside voice has everyone staring at her, it's not long when Monika comes over to see whats wrong…"
  
   m "Is there a problem over here?"

   n "You bet there is! I thought I made it clear last year that we don’t need more members. All it’ll do is make the club less fun…"

   y "Natsuki…"

   play music t10

   m "Natsuki, you do realise this is only one person right? And besides that, He is brand new to Japan, This guy has nothing here and chose us out of all the other clubs to join…"

   "Natsuki seems to change her face from that of anger to one of embarrassment…"

   m "Don’t you think it’s at least fair to give him a chance to make a good impression, you never know if he’d become your friend or not…"

   n "I-.....I guess…"

   m "Who knows, maybe he’ll be a fan of manga, you could introduce him to Parfait girls, as far as I know they dont have that series in America, wouldn’t introducing him to something you like be fun?"

   n "...Y-yea… I suppose I could at least give him a chance…"

   "I'm really surprised how well Monika handled that, usually it's up to Sayori when it comes to de-escalating situations. It only comes to show Monika has grown as a leader, we really should be glad we have her as president."

   s "Speaking of the new guy, where is he?"

   m "He should be here at any moment, I can assume he’s probably very nervous…"

   - Knocking sound effect -

   "Suddenly there's a knock at the door and everyone sushes…"

   m "Quiet everyone, get into position… Okay come in!!!"

   "The door opens slowly as someone walks in…"

   - Shocked Aaron Sprite is shown at the center of the room -
  
   $ m_name = "ALL"

   m "Welcome to the Literature Club!!!"
  
   $ m_name = "Monika"

   "The new guy seems to chuckle nervously, and nod his head in apperchiation. This is much different than what I was expecting, I don't know what i was expecting, but it wasn’t a “Hipster looking Jesus guy”... regardless, he seems nice…"

   - Monika appears to the left of Aaron -

   m "It's so nice to have you here...er-... Goodness im sorry, I don’t think the administrators told me your name…"

   "The new guy looks at Monika, than Sayori, Natsuki, Yuki and Me in that order before looking down at the floor…"
   $ s_name = "???"
   s "..."
   s "..."
   s "..."
   s "... My name is… Aaron…"
   $ s_name = "Sayori"

    "His eyes seem to make a short snap back to Monika as he finally delivers his name. Feels like it might be just me, but it sounded a little kurt."

    m "Well Aaron, It’s a pleasure to have you here, it’s not very often that we get new members. I’m Monika, Founder and President of the Literature Club. How about we introduce you to everybody?"

    "Aaron lifts his head and nods in agreement."

    M "Great, first here we got our Vice-President, Sayori…"

    S "Hi-ya, Welcome to the literature club, you’re gonna have such a good time here, I promise…" 

    "Sayori suddenly embraces Aaron into a hug, as he adopts a bewildered look and appears lost on how to react."
    "Sayori, on the other hand is as warm and bubbly as always, oblivious to any discomfort she might be causing. One of the many reasons I consider her my best friend, honestly."

    M "Hahaha okay- Next we have Yuri-when I comes to good books, she is surely one to give a good suggestion."

    Y "Greetings Aaron, I hope you enjoy Reading and Poetry, we have much of that to go around." 

    "Yuri extends her arm for a handshake, Aaron does the same and offers her a gentle smile." 

    M "Next here we got Natsuki, our very own natural born baking master and Manga Enthusiast!" 

    N "I hope you make yourself useful here, we already got one deadbeat here and we don't need another…"

    "I'm assuming she’s referring to me, I know by now she's only kidding, but Aaron seems to look along the lines of sorry instead of ignoring the joke…"

    M "Now, Natsuki, that's a bit harsh, I'm certain Aaron will fit in the club just fine, you just need to give him time…"

    N "Sorry Monika, whether he’s new or not, I expect everyone here to take the club seriously."
    N "So don’t think just because you’re new or an exchange student, I'll let you take it easy…"

    "Aaron looks at the floor and nods in understanding… he seems to tense up a bit."

    M "Last but certainly not least we have [MC].He was the latest one of us to join, but has already helped the club become a much more welcoming place to be…"

    "How should I introduce myself?"

    - "Hello Aaron, I hope we can learn a lot from each other." #Best

    #https://docs.google.com/document/d/1k0OAJmVHHkvnvKFRv3arFjbHJQZtQDbaMEPZsjzf8AQ/edit#


    - "Sup?" #Netural

    #https://docs.google.com/document/d/1gPRznA060P6ZC8-ozjt3g5CishFWx-5EqqP5MXLqEhU/edit#
    

    - "Why are you so jittery? You scared of something?" #Bad 





    
    
    
    



     


return
# Decompiled by unrpyc: https://github.com/CensoredUsername/unrpyc
